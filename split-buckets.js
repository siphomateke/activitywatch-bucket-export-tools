const fs = require('fs');
const { buckets } = JSON.parse(fs.readFileSync('aw-buckets-export.json', 'utf8'));

Object.keys(buckets).forEach(bucketName => {
  const bucket = buckets[bucketName];
  const filename = `aw-bucket-export_${bucketName}.json`;
  const bucketFile = {
    'buckets': {
      [bucketName]: bucket,
    }
  }
  fs.writeFile(filename, JSON.stringify(bucketFile, null, 2), () => {
    console.log(bucketName);
  });
});
