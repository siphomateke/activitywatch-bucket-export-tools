const fs = require('fs');

/**
 * @typedef BucketEvent
 * @property {string} timestamp
 * @property {number} duration
 * @property {any} data
 *
 * @typedef Bucket
 * @property {string} id
 * @property {string} name
 * @property {string} created
 * @property {string} type
 * @property {string} client
 * @property {string} hostname
 * @property {BucketEvent[]} events
 * 
 * @typedef {{[bucketName: string]: Bucket}} Buckets
 */

/**
 * 
 * @param {string} path 
 * @returns {Buckets}
 */
function loadBucketsFromFile(path) {
  const { buckets } = JSON.parse(fs.readFileSync(path, 'utf8'))
  return buckets;
}

function timestampToDate(timestamp) {
  return new Date(timestamp).valueOf();
}

/** @type {Buckets[]} */
const bucketFiles = [];
bucketFiles.push(loadBucketsFromFile('aw-buckets-export.json'));
bucketFiles.push(loadBucketsFromFile('aw-buckets-export2.json'));

/** @type {Buckets} */
const mergedBuckets = {};
bucketFiles.forEach(bucketFile => {
  const bucketNames = Object.keys(bucketFile);
  bucketNames.forEach(bucketName => {
    const bucket = bucketFile[bucketName];
    if (!(bucketName in mergedBuckets)) {
      mergedBuckets[bucketName] = bucket;
    } else {
      const existingBucket = mergedBuckets[bucketName];
      const existingBucketCreatedDate = timestampToDate(existingBucket.created);
      const bucketCreatedDate = timestampToDate(existingBucket.created);
      if (bucketCreatedDate < existingBucketCreatedDate) {
        existingBucket.created = bucket.created
      }
      existingBucket.events = existingBucket.events.concat(bucket.events);
      if (existingBucket.hostname === 'unknown' && bucket.hostname !== 'unknown') {
        existingBucket.hostname = bucket.hostname;
      }
    }
  });
});

// TODO: Remove duplicates
/**
 * 
 * @param {BucketEvent[]} events 
 */
function removeDuplicateEvents(events) {
  let addedEvents = [];
  let addedEventFingerprints = [];
  events.forEach((event, i) => {
    if (i % 1000 === 0) {
      console.log(`Checking if event is duplicate (${i}/${events.length})`);
    }
    const fingerprint = event.timestamp + event.duration;
    if (!addedEventFingerprints.includes(fingerprint)) {
      addedEvents.push(event);
      addedEventFingerprints.push(fingerprint);
    } else {
      console.log('Removed duplicate event', event);
    }
  });
  return addedEvents;
}

function writeBucketFile(filename, bucketFile) {
  fs.writeFile(filename, JSON.stringify(bucketFile), () => { });
}

function saveBucket(bucket, bucketName) {
  const bucketFile = {
    'buckets': {
      [bucketName]: bucket,
    }
  };
  const filename = `aw-bucket-export-merged_${bucketName}.json`;
  writeBucketFile(filename, bucketFile);
}

const bucketNames = Object.keys(mergedBuckets);
bucketNames.forEach(bucketName => {
  const bucket = mergedBuckets[bucketName]

  // bucket.events = removeDuplicateEvents(bucket.events);

  bucket.events.sort((a, b) => {
    return timestampToDate(b.timestamp) - timestampToDate(a.timestamp);
  });
  console.log(`Re-sorted ${bucketName}`);

  saveBucket(bucket, bucketName);
});

writeBucketFile('aw-buckets-export-merged.json', { 'buckets': mergedBuckets });