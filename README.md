# ActivityWatch Bucket Export Tools

Tools to split and merge ActivityWatch bucket exports.

## Split

Splits a single bucket JSON file that contains multiple buckets (`aw-buckets-export.json`) into multiple files for each bucket.

```bash
node ./split-buckets.js
```

## Merge

Merges multiple full bucket JSON files that were exported at different times. This is a workaround since ActivityWatch crashes for some reason when importing buckets that already exist.

```bash
node ./merge_exports.js
```

The above command will merge `aw-buckets-export.json` and `aw-buckets-export2.json`.